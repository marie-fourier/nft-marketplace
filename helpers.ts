import { ethers } from "ethers";
require("dotenv").config();

const _decimals = 18;

export const contractAddress = String(process.env.CONTRACT_ADDRESS);

export const getContract = async (hre: any) => {
  return await hre.ethers.getContractAt(
    "Marketplace",
    String(process.env.CONTRACT_ADDRESS)
  );
};

export const getAccounts = async (hre: any) => {
  return await hre.ethers.getSigners();
};

export const parseToken = (value: string) => {
  return ethers.BigNumber.from(value).mul(10 ** _decimals);
};

export const getTimestamp = async (hre: any) => {
  const blockNumber = await hre.ethers.provider.getBlockNumber();
  const block = await hre.ethers.provider.getBlock(blockNumber);
  return block.timestamp;
};
