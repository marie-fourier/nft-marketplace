import { expect } from "chai";
import { ethers } from "hardhat";

async function getTimestamp() {
  const blockNumber = await ethers.provider.getBlockNumber();
  const block = await ethers.provider.getBlock(blockNumber);
  return block.timestamp;
}

async function increaseTime(seconds: number) {
  const timestamp = await getTimestamp();
  await setTimestamp(timestamp + seconds);
}

async function setTimestamp(timestamp: number) {
  await ethers.provider.send("evm_mine", [timestamp]);
}

describe("Marketplace", async () => {
  let owner: any, account1: any, account2: any, contract: any, nft: any;

  before(async () => {
    const Contract = await ethers.getContractFactory("Marketplace");
    const NFT = await ethers.getContractFactory("BlueprintsNFT");
    [owner, account1, account2] = await ethers.getSigners();

    nft = await NFT.deploy();
    await nft.deployed();

    contract = await Contract.deploy(nft.address);
    await contract.deployed();

    await nft.transferOwnership(contract.address);
  });

  it("should mint nft", async () => {
    await contract.createItem(1);
    await contract.createItem(2);
    expect(await nft.ownerOf(1)).to.eq(owner.address);
  });

  describe("Sale", async () => {
    it("list should transfer nft to contract and emit event", async () => {
      await nft.approve(contract.address, 1);
      await expect(contract.listItem(1, ethers.utils.parseEther("1")))
        .to.emit(contract, "NewListed")
        .withArgs(owner.address, 1, ethers.utils.parseEther("1"));
    });

    it("only owner can cancel sale", async () => {
      await expect(contract.connect(account1).cancel(1)).to.be.revertedWith(
        "You can't cancel this auction"
      );
    });

    it("should not buy with lower price or if already sold", async () => {
      await expect(
        contract
          .connect(account1)
          .buyItem(1, { value: ethers.utils.parseEther("0.5") })
      ).to.be.revertedWith("Invalid value");

      await expect(
        contract.connect(account1).buyItem(1, {
          value: ethers.utils.parseEther("1"),
        })
      )
        .to.emit(contract, "Buy")
        .withArgs(account1.address, 1, ethers.utils.parseEther("1"));

      expect(await nft.ownerOf(1)).to.eq(account1.address);
    });

    it("should withdraw funds", async () => {
      expect(await contract.unspentBids(owner.address)).to.eq(
        ethers.utils.parseEther("1")
      );
      await expect(() => contract.withdrawUnspentBids()).to.changeEtherBalance(
        owner,
        ethers.utils.parseEther("1")
      );
    });

    it("should not buy sold item", async () => {
      await expect(
        contract.buyItem(1, { value: ethers.utils.parseEther("1") })
      ).to.be.revertedWith("Item already sold");
    });

    it("should not cancel past sales", async () => {
      await expect(contract.cancel(1)).to.be.revertedWith("Item already sold");
    });

    it("should transfer nft back after sale cancel", async () => {
      await nft.connect(account1).approve(contract.address, 1);
      await contract
        .connect(account1)
        .listItem(1, ethers.utils.parseEther("1"));
      await contract.connect(account1).cancel(1);
      expect(await nft.ownerOf(1)).to.eq(account1.address);
    });
  });

  describe("Auction sale", async () => {
    it("should not bid if auction not started", async () => {
      await expect(contract.connect(account1).makeBid(2)).to.be.revertedWith(
        "Auction not started"
      );
    });

    it("list should transfer nft to contract and emit event", async () => {
      await nft.approve(contract.address, 2);
      await expect(contract.listItemOnAuction(2))
        .to.emit(contract, "NewAuction")
        .withArgs(owner.address, 2);
      expect(await nft.ownerOf(2)).to.eq(contract.address);
    });

    it("only owner can cancel auction", async () => {
      await expect(
        contract.connect(account1).cancelAuction(2)
      ).to.be.revertedWith("You can't cancel this auction");
    });

    it("new bid should be higher than previous and emits event", async () => {
      await expect(
        contract
          .connect(account1)
          .makeBid(2, { value: ethers.utils.parseEther("0.1") })
      )
        .to.emit(contract, "NewBid")
        .withArgs(2, account1.address, ethers.utils.parseEther("0.1"));

      await expect(
        contract
          .connect(account2)
          .makeBid(2, { value: ethers.utils.parseEther("0.01") })
      ).to.be.revertedWith("Bid must be higher");
      await contract
        .connect(account2)
        .makeBid(2, { value: ethers.utils.parseEther("0.2") });
      await contract
        .connect(account1)
        .makeBid(2, { value: ethers.utils.parseEther("1") });
      await contract.connect(account2).withdrawUnspentBids();
    });

    it("should not finish auction before end time", async () => {
      await expect(contract.finishAuction(2)).to.be.revertedWith(
        "Auction not ended"
      );
    });

    it("should not cancel or bid if auction ended", async () => {
      await increaseTime(3 * 24 * 60 * 60);
      await expect(contract.cancelAuction(2)).to.be.revertedWith(
        "Auction already ended"
      );
      await expect(
        contract
          .connect(account1)
          .makeBid(2, { value: ethers.utils.parseEther("0.01") })
      ).to.be.revertedWith("Auction already ended");
    });

    it("finishing auction should transfer nft, emit event and increase owners balance", async () => {
      await expect(contract.finishAuction(2))
        .to.emit(contract, "AuctionEnd")
        .withArgs(2, account1.address, ethers.utils.parseEther("1"));
      expect(await nft.ownerOf(2)).to.eq(account1.address);
      expect(await contract.unspentBids(owner.address)).to.eq(
        ethers.utils.parseEther("1")
      );
    });

    it("should not finish auction twice", async () => {
      await expect(contract.finishAuction(2)).to.be.revertedWith(
        "Auction already finished"
      );
    });

    it("canceling auction should send nft back and increase bidders balance", async () => {
      await nft.connect(account1).approve(contract.address, 2);
      await contract.connect(account1).listItemOnAuction(2);
      await contract
        .connect(account2)
        .makeBid(2, { value: ethers.utils.parseEther("1") });
      await contract.connect(account1).cancelAuction(2);

      expect(await nft.ownerOf(2)).to.eq(account1.address);
      expect(await contract.unspentBids(account2.address)).to.eq(
        ethers.utils.parseEther("1")
      );
    });

    it("should not bid or cancel if auction cancelled", async () => {
      await expect(
        contract.connect(account1).cancelAuction(2)
      ).to.be.revertedWith("You can't cancel this auction");
      await expect(
        contract.makeBid(2, { value: ethers.utils.parseEther("1") })
      ).to.be.revertedWith("Auction not started");
    });

    it("should not finish auction if it has only one bidder", async () => {
      await nft.connect(account1).approve(contract.address, 2);
      await contract.connect(account1).listItemOnAuction(2);
      await contract
        .connect(account2)
        .makeBid(2, { value: ethers.utils.parseEther("1") });
      await contract
        .connect(account2)
        .makeBid(2, { value: ethers.utils.parseEther("2") });
      await contract
        .connect(account2)
        .makeBid(2, { value: ethers.utils.parseEther("5") });
      await increaseTime(3 * 24 * 60 * 60);
      await expect(
        contract.connect(account1).finishAuction(2)
      ).to.be.revertedWith("Auction has not enough bidders");
    });
  });
});
