// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./NFT.sol";

contract Marketplace is Ownable {
  struct ListedAuctionItem {
    address owner;
    uint256 startTime;
    uint256 endTime;
    uint256 highestBid;
    address highestBidder;
    bool finished;
    bool legit; // has more than two bidders
  }
  struct ListedItem {
    address owner;
    uint256 value;
    bool sold;
  }

  event NewAuction(address indexed _from, uint256 indexed _tokenId);
  event NewBid(uint256 indexed _tokenId, address indexed _from, uint256 value);
  event NewListed(address indexed _from, uint256 indexed _tokenId, uint256 value);
  event Buy(address indexed _addr, uint256 indexed _tokenId, uint256 value);
  event AuctionEnd(uint256 indexed _tokenId, address indexed _to, uint256 value);

  BlueprintsNFT private nftContract;
  mapping(uint256 => ListedAuctionItem) private auctionItems;
  mapping(uint256 => ListedItem) private listedItems;
  mapping(address => uint256) public unspentBids;

  constructor(address _nftContract) {
    nftContract = BlueprintsNFT(_nftContract);
  }
  
  function createItem(uint256 _tokenId) external {
    _mint(msg.sender, _tokenId);
  }

  function listItem(uint256 _tokenId, uint256 _value) external {
    nftContract.transferFrom(msg.sender, address(this), _tokenId);
    ListedItem storage item = listedItems[_tokenId];
    item.owner = msg.sender;
    item.sold = false;
    item.value = _value;
    emit NewListed(msg.sender, _tokenId, _value);
  }

  function buyItem(uint256 _tokenId) external payable {
    ListedItem storage item = listedItems[_tokenId];
    require(!item.sold, "Item already sold");
    require(msg.value >= item.value, "Invalid value");
    item.sold = true;
    nftContract.transferFrom(address(this), msg.sender, _tokenId);
    unspentBids[item.owner] += msg.value;
    emit Buy(msg.sender, _tokenId, msg.value);
  }

  function cancel(uint256 _tokenId) external {
    require(listedItems[_tokenId].owner == msg.sender, "You can't cancel this auction");
    require(!listedItems[_tokenId].sold, "Item already sold");
    nftContract.transferFrom(address(this), msg.sender, _tokenId);
  }
  
  function listItemOnAuction(uint256 _tokenId) external {
    nftContract.transferFrom(msg.sender, address(this), _tokenId);
    ListedAuctionItem storage auctionItem = auctionItems[_tokenId];
    auctionItem.owner = msg.sender;
    auctionItem.startTime = block.timestamp;
    auctionItem.endTime = block.timestamp + 3 days;
    auctionItem.highestBid = 0;
    auctionItem.highestBidder = msg.sender;
    auctionItem.finished = false;
    auctionItem.legit = false;
    emit NewAuction(msg.sender, _tokenId);
  }

  function makeBid(uint256 _tokenId) external payable {
    ListedAuctionItem storage item = auctionItems[_tokenId];
    require(item.startTime > 0, "Auction not started");
    require(item.endTime > block.timestamp, "Auction already ended");
    require(msg.value > item.highestBid, "Bid must be higher");

    uint256 lastBid = item.highestBid;
    address lastBidder = item.highestBidder;
    unspentBids[lastBidder] += lastBid;

    if (lastBid > 0 && lastBidder != msg.sender) {
      item.legit = true;
    }

    item.highestBid = msg.value;
    item.highestBidder = msg.sender;
    emit NewBid(_tokenId, msg.sender, msg.value);
  }

  function finishAuction(uint256 _tokenId) external {
    ListedAuctionItem storage item = auctionItems[_tokenId];
    require(item.endTime < block.timestamp, "Auction not ended");
    require(!item.finished, "Auction already finished");
    require(item.legit, "Auction has not enough bidders");
    item.finished = true;
    nftContract.transferFrom(address(this), item.highestBidder, _tokenId);
    unspentBids[item.owner] += item.highestBid;
    emit AuctionEnd(_tokenId, item.highestBidder, item.highestBid);
  }

  function cancelAuction(uint256 _tokenId) external {
    ListedAuctionItem storage item = auctionItems[_tokenId];
    require(item.owner == msg.sender, "You can't cancel this auction");
    require(item.endTime > block.timestamp, "Auction already ended");
    item.startTime = 0; // users can't make bid
    item.owner = address(0); // owner can't cancel twice
    unspentBids[item.highestBidder] += item.highestBid;
    nftContract.transferFrom(address(this), msg.sender, _tokenId);
  }

  function withdrawUnspentBids() external {
    uint256 value = unspentBids[msg.sender];
    unspentBids[msg.sender] = 0;
    payable(msg.sender).transfer(value);
  }

  function _mint(address _to, uint256 _tokenId) internal {
    nftContract.mint(_to, _tokenId);
  }
}