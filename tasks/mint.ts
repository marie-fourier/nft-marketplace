import { task } from "hardhat/config";
import { getContract } from "../helpers";

interface TaskArgs {
  id: Number;
}

task("mint")
  .addParam("id", "Token id")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const contract = await getContract(hre);
    try {
      await contract.createItem(taskArgs.id);
      console.log("Minted NFT");
    } catch (err) {
      console.log("Error minting");
    }
  });
